from django import forms
from .models import PersonExt, BaseACL

#my forms here

class PersonExtForm(forms.ModelForm):
    model = PersonExt
    fields = ['slack_api_token', 'slack_chat_room', 'default_base_name', 'default_server_name', 'slack_notify_flag', 'email_notify_flag']

class BaseACL(forms.ModelForm):
    model = BaseACL
    fields = ['base_name', 'base_server', 'shared_to']
