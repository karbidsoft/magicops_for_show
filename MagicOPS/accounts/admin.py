from django.contrib import admin
from .models import BaseACL, PersonExt

# Register your models here.

class PersonExtAdmin(admin.ModelAdmin):
    search_fields = ('default_base_name', 'default_server_name', 'user_ext_id')
    list_filter = ('default_base_name', 'default_server_name', 'slack_notify_flag', 'email_notify_flag')
    list_display = ('default_base_name', 'default_server_name', 'slack_api_token', 'slack_chat_room', 'user_ext_id')
    list_display_links = ('default_base_name', 'default_server_name', 'slack_api_token', 'slack_chat_room', 'user_ext_id')
    list_per_page = 50

class BaseACLAdmin(admin.ModelAdmin):
    search_fields = ('base_name', 'base_server', 'shared_to')
    list_filter = ('base_name', 'base_server')
    list_display_links = ('base_name', 'base_server', 'shared_to')
    list_display = ('base_name', 'base_server', 'shared_to')
    list_per_page = 50

admin.site.register(PersonExt, PersonExtAdmin)
admin.site.register(BaseACL, BaseACLAdmin)
