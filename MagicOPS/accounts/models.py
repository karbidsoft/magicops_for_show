from django.db import models
from django.contrib.auth.models import User
# from django.contrib.auth.models import AbstractBaseUser
# from django.contrib.auth.models import PermissionsMixin
# from django.contrib.auth.models import BaseUserManager

# Create your models here.
# class PersonGroup(models.Model):
#     group_name = models.CharField(verbose_name="Group Name", max_length=16)
#     group_desc = models.CharField(verbose_name="Short Description", max_length=150)


class PersonExt(models.Model):
    class Meta:
        verbose_name = "Дополнительная настройка пользователя"
        verbose_name_plural = "Дополнительные настройки пользователей"
        db_table = "magicops_person_ext"
#    person_group = models.ForeignKey(PersonGroup, on_delete=models.CASCADE)
    user_ext_id = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Apply for User", default=None)
    slack_api_token = models.CharField(verbose_name="Api Tocken for Slack", max_length=250)
    slack_chat_room = models.CharField(verbose_name="Slack Chat Room", max_length=250)
    slack_notify_flag = models.BooleanField(verbose_name="Enable Slack Notifications", default=False)
    email_notify_flag = models.BooleanField(verbose_name="Enable Email Notifications", default=False)
    default_base_name = models.CharField(verbose_name="Default 1C Database", max_length=100)
    default_server_name = models.CharField(verbose_name="Default 1C Server", max_length=32)

    def __str__(self):
        return self.user_ext_id

#    work_server_name = models.CharField(verbose_name="Work Server DNS Name", max_length=16)

class BaseACL(models.Model):
    class Meta:
        verbose_name = "Access List Control"
        verbose_name_plural = "Access List's Control's"
        db_table = "magicops_base_acl"
    base_name = models.CharField(verbose_name="Database for Sharing", max_length=100)
    base_server = models.CharField(verbose_name="Database Location Server", max_length=32)
    shared_to = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Shared to User", default=None)

    def __str__(self):
        return "Srvr = %s ; Ref = %s" % (self.base_server, self.base_name)


# class UserManager(BaseUserManager):
#
#     def create_user(self, email, password=None):
#         if not email:
#             raise ValueError('Email непременно должен быть указан')
#
#         user = self.model(
#             email=UserManager.normalize_email(email),
#         )
#
#         user.set_password(password)
#         user.save(using=self._db)
#         return user
#
#     def create_superuser(self, email, password):
#         user = self.create_user(email, password)
#         user.is_admin = True
#         user.save(using=self._db)
#         return user
#
#
# class ExtUser(AbstractBaseUser, PermissionsMixin):
#
#     email = models.EmailField(
#         'Электронная почта',
#         max_length=255,
#         unique=True,
#         db_index=True
#     )
#     avatar = models.ImageField(
#         'Аватар',
#         blank=True,
#         null=True,
#         upload_to="users/avatars/%Y/%m/%d"
#     )
#     firstname = models.CharField(
#         'Фамилия',
#         max_length=40,
#         null=True,
#         blank=True
#     )
#     lastname = models.CharField(
#         'Имя',
#         max_length=40,
#         null=True,
#         blank=True
#     )
#     middlename = models.CharField(
#         'Отчество',
#         max_length=40,
#         null=True,
#         blank=True
#     )
#     register_date = models.DateField(
#         'Дата регистрации',
#         auto_now_add=True
#     )
#     is_active = models.BooleanField(
#         'Активен',
#         default=True
#     )
#     is_staff = models.BooleanField(
#         'Статус персонала',
#         default=False
#     )
#     is_admin = models.BooleanField(
#         'Суперпользователь',
#         default=False
#     )
#     slack_api_token = models.CharField(
#         verbose_name="Api Tocken for Slack",
#         max_length=250
#     )
#     slack_chat_room = models.CharField(
#         verbose_name="Slack Chat Room",
#         max_length=250
#     )
#     slack_notify_flag = models.BooleanField(
#         verbose_name="Enable Slack Notifications",
#         default=False
#     )
#     email_notify_flag = models.BooleanField(
#         verbose_name="Enable Email Notifications",
#         default=False
#     )
#
#     # Этот метод обязательно должен быть определён
#     def get_full_name(self):
#         return self.email
#
#     # Требуется для админки
#     @property
#     def is_staff(self):
#         return self.is_staff
#
#     def get_short_name(self):
#         return self.email
#
#     def __str__(self):
#         return self.email
#
#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELDS = []
#
#     objects = UserManager()
#
#     class Meta:
#         verbose_name = 'Пользователь'
#         verbose_name_plural = 'Пользователи'
