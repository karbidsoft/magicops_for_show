from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'MagicOPS.accounts'
    verbose_name = 'Дополнительные настройки пользователей'
