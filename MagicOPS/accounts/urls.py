from django.conf.urls import url
from MagicOPS.accounts import views

urlpatterns = [
#    url(r'^profile/$', views.profile, name="mo-profile")
    url(r'^accounts/logout/$', views.logout_action, name='logout'),
]
