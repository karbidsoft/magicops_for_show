ALLOWED_HOSTS = ['']

DEFAULT_FROM_EMAIL = ''

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.sqlite3',
       'NAME': os.path.join(BASE_DIR, 'MagicOPS.sqlite3'),
   }
}
