from django.apps import AppConfig


class FrontConfig(AppConfig):
    name = 'MagicOPS.front'
