from django.conf.urls import include, url

from MagicOPS.front import views

urlpatterns = [
    url(r'^$', views.index, name="mo-index"),
    url(r'^vrd/$', views.webservices, name="mo-webservices"),
    url(r'^xml/$', views.connectarams, name="mo-connectparams"),
    # url(r'^checks/cron_preview/$', views.cron_preview),
    # url(r'^checks/([\w-]+)/', include(check_urls)),
    # url(r'^integrations/', include(channel_urls)),
    #
    # url(r'^docs/$', views.docs, name="hc-docs"),
    # url(r'^docs/api/$', views.docs_api, name="hc-docs-api"),
    # url(r'^about/$', views.about, name="hc-about"),
    # url(r'^privacy/$', views.privacy, name="hc-privacy"),
    # url(r'^terms/$', views.terms, name="hc-terms"),
]