import os
from django.shortcuts import render
from django.template.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib import auth

from MagicOPS.settings import BASE_DIR

from .forms import EditFileForm

# Create your views here.

@login_required
def index(request):
    #todo Create this view and template

    auth_user_object = auth.get_user(request)

    if request.method == 'POST':
        context = {}
        context.update(csrf(request))
        form = EditFileForm(request.POST)
        if form.is_valid():
            try:
                file_path = os.path.join(BASE_DIR, 'base/' + str(auth_user_object) + '/default.vrd')
                f = open(file_path, 'w')
                f.write(form.cleaned_data["file_body"])
                f.close()
            except:
                pass
    else:
        try:
            file_path = os.path.join(BASE_DIR, 'base/' + str(auth_user_object) + '/default.vrd')
            f = open(file_path, 'r')
            file_body = f.read()
            f.close()
            form = EditFileForm({'file_body': file_body})
        except:
            form = EditFileForm()
    context = {
        'form': form,
        'auth_user': auth_user_object,
        'request': request
    }

    return render(request, 'editfile.html', context)

@login_required
def webservices(request):
    #todo Create this view and template

    auth_user_object = auth.get_user(request)

    if request.method == 'POST':
        context = {}
        context.update(csrf(request))
        form = EditFileForm(request.POST)
        if form.is_valid():
            try:
                file_path = os.path.join(BASE_DIR, 'base/' + str(auth_user_object) + '/default.vrd')
                f = open(file_path, 'w')
                f.write(form.cleaned_data["file_body"])
                f.close()
            except:
                pass
    else:
        try:
            file_path = os.path.join(BASE_DIR, 'base/' + str(auth_user_object) + '/default.vrd')
            f = open(file_path, 'r')
            file_body = f.read()
            f.close()
            form = EditFileForm({'file_body': file_body})
        except:
            form = EditFileForm()
    context = {
        'form': form,
        'auth_user': auth_user_object,
        'request': request
    }

    return render(request, 'editfile.html', context)

@login_required
def connectarams(request):
    #todo Create this view and template

    auth_user_object = auth.get_user(request)

    if request.method == 'POST':
        context = {}
        context.update(csrf(request))
        form = EditFileForm(request.POST)
        if form.is_valid():
            try:
                file_path = os.path.join(BASE_DIR, 'cp/' + str(auth_user_object) + '/ConnectParams.xml')
                f = open(file_path, 'w')
                f.write(form.cleaned_data["file_body"])
                f.close()
            except:
                pass
    else:
        try:
            file_path = os.path.join(BASE_DIR, 'cp/' + str(auth_user_object) + '/ConnectParams.xml')
            f = open(file_path, 'r')
            file_body = f.read()
            f.close()
            form = EditFileForm({'file_body': file_body})
        except:
            form = EditFileForm()
    context = {
        'form': form,
        'auth_user': auth_user_object,
        'request': request
    }

    return render(request, 'editfile.html', context)
