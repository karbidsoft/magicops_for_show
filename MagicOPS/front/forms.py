from django import forms
from codemirror import CodeMirrorTextarea

class EditFileForm(forms.Form):

    codemirror_widget = CodeMirrorTextarea(mode="xml", theme="monokai", config={'lineNumbers': True},  )
    file_body = forms.CharField(label='Content:', widget=codemirror_widget)
